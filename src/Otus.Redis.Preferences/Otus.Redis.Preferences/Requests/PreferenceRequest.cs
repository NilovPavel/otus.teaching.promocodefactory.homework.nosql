﻿namespace Otus.Redis.Preferences.Requests
{
    public class PreferenceRequest
    {
        public string Name { get; set; }
    }
}