﻿using System;

namespace Otus.Redis.Preferences.Responses
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}