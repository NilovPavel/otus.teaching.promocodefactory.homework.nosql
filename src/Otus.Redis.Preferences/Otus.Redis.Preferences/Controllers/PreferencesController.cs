﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Redis.Preferences.Requests;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using StackExchange.Redis;

//https://developer.redis.com/develop/dotnet/aspnetcore/rate-limiting/fixed-window
namespace Otus.Redis.Preferences.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {
        private readonly IDatabase _db;
        public PreferencesController(IConnectionMultiplexer mux)
        {
            _db = mux.GetDatabase();
        }

        [HttpGet]
        public async Task<IActionResult> AddPreference(string guidStr)
        {
            RedisValue redisValue = _db.StringGet(new RedisKey(guidStr));
            if (!redisValue.HasValue)
                return NotFound(guidStr);
            return Ok(redisValue.ToString());
        }

        [HttpPost]
        public async Task<IActionResult> GetPreference([FromBody] PreferenceRequest preference)
        {
            string redisKey = Guid.NewGuid().ToString();
            Task<bool> status = _db.StringSetAsync(redisKey, preference.Name);
            if (status.IsCompleted)
            { BadRequest(redisKey); };
            return Ok(redisKey);
        }
    }
}
